import { Nav } from './nav'

export default function Home() {
  return (
    <main className="flex items-center justify-between bg-white">
      <div className="h-[30rem] w-full relative bg-[url('/background/homepage-hero-gradient.png')] bg-cover">
        <div className='z-10 py-3 flex justify-center'>
          <Nav />
        </div>


      </div>


    </main>
  )
}

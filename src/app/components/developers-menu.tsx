'use client'
import Image from 'next/image';
import { forwardRef } from 'react';


export const DevelopersMenu =  forwardRef<HTMLElement>(function DevelopersMenu(props, ref) {
    return (
        <section ref={ref}  className='w-100%'>

            <div className='px-8 pt-[2.125rem] pb-[1.3125rem] flex flex-col gap-3'>

                <div className='flex flex-col gap-y-8'>
                    <div className='flex gap-2.5 items-start'>
                        <Image className='mt-0.5' src='/icon/stripe-press.svg' width="16" height="16" alt="rocket-icon" />
                        <div className='flex flex-col'>
                            <p className='text-[#0A2540] text-sm font-medium tracking-[0.0125rem]'>Documentation </p>
                            <p className='text-[#0A2540] text-sm font-light tracking-[0.0125rem]'>Start integrating Stripe’s products and tools </p>
                        </div>
                    </div>

                    <div className='flex gap-11'>
                        <div className='flex flex-col gap-2'>
                            <p className='text-[#727F96] font-helvetica font-medium text-[0.8125rem] leading-5 tracking-wide uppercase'>Get started</p>
                            <p className='text-[#0A2540] font-helvetica font-light text-sm tracking-[0.0125em]'>Prebuilt checkout </p>
                            <p className='text-[#0A2540] font-helvetica font-light text-sm tracking-[0.0125em]'>Libraries and SDKs  </p>
                            <p className='text-[#0A2540] font-helvetica font-light text-sm tracking-[0.0125em]'>Plugins   </p>
                            <p className='text-[#0A2540] font-helvetica font-light text-sm tracking-[0.0125em]'>Code samples </p>
                            
                        </div>

                        <div className='flex flex-col gap-2'>
                            <p className='text-[#727F96] font-helvetica font-medium text-[0.8125rem] leading-5 tracking-wide uppercase'>Guides</p>
                            <p className='text-[#0A2540] font-helvetica font-light text-sm tracking-[0.0125em]'>Accept online payments  </p>
                            <p className='text-[#0A2540] font-helvetica font-light text-sm tracking-[0.0125em]'>Manage subscriptions   </p>
                            <p className='text-[#0A2540] font-helvetica font-light text-sm tracking-[0.0125em]'>Send payments </p>
                            <p className='text-[#0A2540] font-helvetica font-light text-sm tracking-[0.0125em]'>Set up in-person payments   </p>
                            
                        </div>
                    </div>

                </div>
            </div>

            <div className='p-1'>
                <div className='p-7  rounded bg-[#EFF3F9] flex flex-col gap-3'>
                    <p className='font-helvetica text-[#4F5B76] text-[0.775rem] font-medium leading-5 uppercase tracking-wide'>By stage</p>
                    <div className='grid grid-cols-2 gap-x-8 gap-y-3.5'>
                        <div className='flex gap-2.5'>
                            <Image src='/icon/full-api-reference.svg' width="16" height="16" alt="rocket-icon" />
                            <p className='text-[#0A2540] text-sm font-medium tracking-[0.0125rem]'>Full API Reference </p>
                        </div>

                        <div className='flex gap-2.5'>
                            <Image src='/icon/api-status.svg' width="16" height="16" alt="rocket-icon" />
                            <p className='text-[#0A2540] text-sm font-medium tracking-[0.0125rem]'>API Status  </p>
                        </div>

                        <div className='flex gap-2.5'>
                            <Image src='/icon/api-changelog.svg' width="16" height="16" alt="rocket-icon" />
                            <p className='text-[#0A2540] text-sm font-medium tracking-[0.0125rem]'>API Changelog </p>
                        </div>

                        <div className='flex gap-2.5'>
                            <Image src='/icon/build-a-stripe-app.svg' width="16" height="16" alt="rocket-icon" />
                            <p className='text-[#0A2540] text-sm font-medium tracking-[0.0125rem]'>Build a Stripe App </p>
                        </div>


                    </div>
                </div>
            </div>
        </section >
    )
})

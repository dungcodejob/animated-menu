'use client'
import Image from 'next/image';
import { forwardRef } from 'react';
import styles from '../solutions-menu.module.css';

export const SolutionsMenu =  forwardRef<HTMLElement>(function SolutionsMenu(props, ref) {
    return (
        <section  ref={ref} className='w-100%'>
            <div className='px-8 pt-[2.125rem] pb-[1.3125rem] flex flex-col gap-3'>
                <p className='font-helvetica text-[#727F96] text-[0.775rem] font-medium leading-5 uppercase tracking-wide'>By stage</p>
                <div className='flex gap-8'>
                    <div className='flex-grow flex gap-2.5'>
                        <Image src='/icon/rocket.svg' width="16" height="16" alt="rocket-icon" />
                        <p className='text-[#0A2540] text-sm font-medium tracking-[0.0125rem]'>Startups</p>
                    </div>
                    <div className='flex-grow flex gap-2.5'>
                        <Image src='/icon/house.svg' width="16" height="16" alt="rocket-icon" />
                        <p className='text-[#0A2540] text-sm font-medium tracking-[0.0125rem]'>Enterprises </p>
                    </div>
                </div>
            </div>
            <div className={styles['solutions-menu__pseudo']}></div>
            <div className='px-8 pt-[1.375rem] pb-[1.3125rem] flex flex-col gap-3'>
                <p className='font-helvetica text-[#727F96] text-[0.775rem] font-medium leading-5 uppercase tracking-wide'>By Use Case</p>
                <div className='grid grid-cols-2 gap-x-8 gap-y-3.5'>
                    <div className='flex gap-2.5'>
                        <Image src='/icon/saas.svg' width="16" height="16" alt="rocket-icon" />
                        <p className='text-[#0A2540] text-sm font-medium tracking-[0.0125rem]'>SaaS</p>
                    </div>

                    <div className='flex gap-2.5'>
                        <Image src='/icon/creator-economy.svg' width="16" height="16" alt="rocket-icon" />
                        <p className='text-[#0A2540] text-sm font-medium tracking-[0.0125rem]'>Creator Economy </p>
                    </div>

                    <div className='flex gap-2.5'>
                        <Image src='/icon/platforms.svg' width="16" height="16" alt="rocket-icon" />
                        <p className='text-[#0A2540] text-sm font-medium tracking-[0.0125rem]'>Platforms</p>
                    </div>

                    <div className='flex gap-2.5'>
                        <Image src='/icon/embedded-finance.svg' width="16" height="16" alt="rocket-icon" />
                        <p className='text-[#0A2540] text-sm font-medium tracking-[0.0125rem]'>Embedded Finance </p>
                    </div>

                    <div className='flex gap-2.5'>
                        <Image src='/icon/ecommerce.svg' width="16" height="16" alt="rocket-icon" />
                        <p className='text-[#0A2540] text-sm font-medium tracking-[0.0125rem]'>Ecommerce </p>
                    </div>
                    <div className='flex gap-2.5'>
                        <Image src='/icon/global-businesses.svg' width="16" height="16" alt="rocket-icon" />
                        <p className='text-[#0A2540] text-sm font-medium tracking-[0.0125rem]'>Global Businesses </p>
                    </div>
                    <div className='flex gap-2.5'>
                        <Image src='/icon/marketplaces.svg' width="16" height="16" alt="rocket-icon" />
                        <p className='text-[#0A2540] text-sm font-medium tracking-[0.0125rem]'>Marketplaces  </p>
                    </div>

                    <div className='flex gap-2.5'>
                        <Image src='/icon/finance-automation.svg' width="16" height="16" alt="rocket-icon" />
                        <p className='text-[#0A2540] text-sm font-medium tracking-[0.0125rem]'>Finance Automation </p>
                    </div>

                    <div className='flex gap-2.5'>
                        <Image src='/icon/crypto.svg' width="16" height="16" alt="rocket-icon" />
                        <p className='text-[#0A2540] text-sm font-medium tracking-[0.0125rem]'>Crypto </p>
                    </div>

                </div>
            </div>

            <div className='p-1'>
                <div className='p-7  rounded bg-[#EFF3F9] flex flex-col gap-3'>
                    <p className='font-helvetica text-[#4F5B76] text-[0.775rem] font-medium leading-5 uppercase tracking-wide'>By stage</p>
                    <div className='grid grid-cols-2 gap-x-8 gap-y-3.5'>
                        <div className='flex gap-2.5'>
                            <Image src='/icon/app-marketplace.svg' width="16" height="16" alt="rocket-icon" />
                            <p className='text-[#0A2540] text-sm font-medium tracking-[0.0125rem]'>App Marketplace </p>
                        </div>

                        <div className='flex gap-2.5'>
                            <Image src='/icon/partner-ecosystem.svg' width="16" height="16" alt="rocket-icon" />
                            <p className='text-[#0A2540] text-sm font-medium tracking-[0.0125rem]'>Partner Ecosystem  </p>
                        </div>

                        <div className='flex gap-2.5'>
                            <Image src='/icon/professional-services.svg' width="16" height="16" alt="rocket-icon" />
                            <p className='text-[#0A2540] text-sm font-medium tracking-[0.0125rem]'>Professional Services </p>
                        </div>


                    </div>
                </div>
            </div>
        </section >
    )
})

'use client'
import Image from 'next/image';
import { forwardRef } from 'react';

export const ResourcesMenu =  forwardRef<HTMLElement>(function ResourcesMenu(props, ref) {
    return (
        <section  ref={ref} className='w-100%'>

            <div className='px-8 pt-[2.125rem] pb-6 flex flex-col gap-3'>

                <div className='grid grid-cols-2 gap-x-8 gap-y-3.5'>
                    <div className='flex gap-2.5'>
                        <Image src='/icon/support-center.svg' width="16" height="16" alt="rocket-icon" />
                        <p className='text-[#0A2540] text-sm font-medium tracking-[0.0125rem]'>Support Center </p>
                    </div>

                    <div className='flex gap-2.5'>
                        <Image src='/icon/blog.svg' width="16" height="16" alt="rocket-icon" />
                        <p className='text-[#0A2540] text-sm font-medium tracking-[0.0125rem]'>Blog </p>
                    </div>

                    <div className='flex gap-2.5'>
                        <Image src='/icon/support-plans.svg' width="16" height="16" alt="rocket-icon" />
                        <p className='text-[#0A2540] text-sm font-medium tracking-[0.0125rem]'>Support Plans </p>
                    </div>

                    <div className='flex gap-2.5'>
                        <Image src='/icon/annual-conference.svg' width="16" height="16" alt="rocket-icon" />
                        <p className='text-[#0A2540] text-sm font-medium tracking-[0.0125rem]'>Annual Conference  </p>
                    </div>

                    <div className='flex gap-2.5'>
                        <Image src='/icon/guides.svg' width="16" height="16" alt="rocket-icon" />
                        <p className='text-[#0A2540] text-sm font-medium tracking-[0.0125rem]'>Guides </p>
                    </div>
                    <div className='flex gap-2.5'>
                        <Image src='/icon/marketplaces.svg' width="16" height="16" alt="rocket-icon" />
                        <p className='text-[#0A2540] text-sm font-medium tracking-[0.0125rem]'>Contact Sales </p>
                    </div>

                    <div className='flex gap-2.5'>
                        <Image src='/icon/finance-automation.svg' width="16" height="16" alt="rocket-icon" />
                        <p className='text-[#0A2540] text-sm font-medium tracking-[0.0125rem]'>Customer Stories </p>
                    </div>

                </div>
            </div>

            <div className='p-1'>
                <div className='p-7  rounded bg-[#EFF3F9] flex flex-col gap-3'>
                    <p className='font-helvetica text-[#4F5B76] text-[0.775rem] font-medium leading-5 uppercase tracking-wide'>By stage</p>
                    <div className='grid grid-cols-2 gap-x-8 gap-y-3.5'>
                        <div className='flex gap-2.5'>
                            <Image src='/icon/app-marketplace.svg' width="16" height="16" alt="rocket-icon" />
                            <p className='text-[#0A2540] text-sm font-medium tracking-[0.0125rem]'>Jobs  </p>
                        </div>

                        <div className='flex gap-2.5'>
                            <Image src='/icon/stripe-press.svg' width="16" height="16" alt="rocket-icon" />
                            <p className='text-[#0A2540] text-sm font-medium tracking-[0.0125rem]'>Stripe Press </p>
                        </div>

                        <div className='flex gap-2.5'>
                            <Image src='/icon/newsroom.svg' width="16" height="16" alt="rocket-icon" />
                            <p className='text-[#0A2540] text-sm font-medium tracking-[0.0125rem]'>Newsroom </p>
                        </div>

                        <div className='flex gap-2.5'>
                            <Image src='/icon/become-a-partner.svg' width="16" height="16" alt="rocket-icon" />
                            <p className='text-[#0A2540] text-sm font-medium tracking-[0.0125rem]'>Become a Partner</p>
                        </div>


                    </div>
                </div>
            </div>
        </section >
    )
})

"use strict";
import clsx from 'clsx';
import React from 'react';


type Props = {
    index: number;
    hovering: number | null;
    children: React.ReactNode
}

export const SlideWrapper: React.FC<Props> = ({index, hovering,children}) => {
    return <div
      className={clsx(
        "absolute w-full transition-all duration-300",
        hovering === index ? "opacity-100" : "opacity-0 pointer-events-none",
         hovering === index || hovering === null ? "transform-none"
          : hovering! > index
          ? "-translate-x-24"
          : "translate-x-24"
      )}
    >
      {children}
    </div>
}
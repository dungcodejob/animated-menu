'use client'
import React, { useRef, useState } from 'react';
import { DevelopersMenu } from './components/developers-menu';
import { ResourcesMenu } from './components/resources-menu';
import { SlideWrapper } from "./components/slide-wrapper";
import { SolutionsMenu } from './components/solutions-menu';

export const Nav: React.FC = () => {

    const [hovering, setHovering] = useState<number | null>(null);
    const [popoverLeft, setPopoverLeft] = useState<number | null>(null);
    const [popoverHeight, setPopoverHeight] = useState<number | null>(null);
    const navRef = useRef<HTMLElement | null>(null);
    const menuRefs = useRef<(HTMLElement | null)[]>([]);


    const focusMenu = (index: number, el: HTMLElement) => {
        setHovering(index);
        const left = calculateLeft(el);
        setPopoverLeft(left);

        const menuElement = menuRefs.current[index];
        if (menuElement) {
            setPopoverHeight(menuElement.offsetHeight);
        }
    }


    const calculateLeft = (currentTarget: HTMLElement) => {
        const targetLeft = currentTarget.getBoundingClientRect().left
        const navLeft = navRef.current ? navRef.current.getBoundingClientRect().left : 0;

        return targetLeft - navLeft - 200;
    }



    return (
        <nav ref={navRef}
            className='relative mx-[8.125rem] px-4'
            onMouseEnter={() => setHovering(null)}>
            <div className='px-4 py-3 flex justify-between items-center'>
                <ul className='flex'>
                    <li className='px-5 py-[0.78125rem]' ><a href="/">Product</a></li>
                    <li className='px-5 py-[0.78125rem]' 
                    onFocus={(event) => focusMenu(0, event.currentTarget)}
                    onMouseEnter={(event) => focusMenu(0, event.currentTarget)}><a href="/">
                        Solutions</a></li>
                    <li className='px-5 py-[0.78125rem]' 
                    onFocus={(event) => focusMenu(1, event.currentTarget)}
                    onMouseEnter={(event) => focusMenu(1, event.currentTarget)}><a href="/">Developers</a></li>
                    <li className='px-5 py-[0.78125rem]' 
                    onFocus={(event) => focusMenu(2, event.currentTarget)}
                    onMouseEnter={(event) => focusMenu(2, event.currentTarget)}><a href="/">Resources</a></li>
                </ul>
            </div>

            <div className='relative'>
                {
                    typeof hovering === "number" && (
                        <div style={{
                            left: `${popoverLeft}px`,
                            height: `${popoverHeight}px`,

                        }} className="transition-all duration-300 absolute shadow bg-white rounded w-[600px]">

                            <SlideWrapper index={0} hovering={hovering}>
                                <SolutionsMenu ref={element => menuRefs.current[0] = element} />
                            </SlideWrapper>
                            <SlideWrapper index={1} hovering={hovering}>
                                <DevelopersMenu ref={element => menuRefs.current[1] = element} />
                            </SlideWrapper>
                            <SlideWrapper index={2} hovering={hovering}>
                                <ResourcesMenu ref={element => menuRefs.current[2] = element} />
                            </SlideWrapper>


                        </div>
                    )
                }
            </div>


        </nav>
    )
}


